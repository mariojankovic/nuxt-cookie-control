 ```javascript
 in template:
  <CookieControl />

  or

  <CookieControl>
    <h3>Title</h3>
    <p>Your text goes here</p>
  </CookieControl>

  to open cookie modal anywhere:
  $cookies.modal = true
  or
  this.$cookies.modal = true

  props: locale (default = 'en')

  Locales: 'en, de, es, hr';

  nuxt.config.js:
  modules: [
    //Short
    'nuxt-cookie-control'

    //Long
    ['nuxt-cookie-control', {
      //options
      colors: {
        barTextColor: '#fff',
        modalOverlay: '#000',
        barBackground: '#000',
        barButtonColor: '#000',
        modalTextColor: '#000',
        modalBackground: '#fff',
        modalOverlayOpacity: 0.8;
        modalButtonColor: '#fff',
        modalUnsavedColor: '#fff',
        barButtonHoverColor: '#fff',
        barButtonBackground: '#fff',
        modalButtonHoverColor: '#fff',
        modalButtonBackground: '#000',
        barButtonHoverBackground: '#333',
        checkboxActiveBackground: '#000',
        checkboxUnactiveBackground: '#000',
        modalButtonHoverBackground: '#333',
        checkboxDisabledBackground: '#ddd',
        checkboxActiveCircleBackground: '#fff',
        checkboxUnactiveCircleBackground: '#fff',
        checkboxDisabledCircleBackground: '#fff',
      },

      text: {
        //If multilanguage and different form default
        locale: {
          en: {
            barTitle: 'Cookies Different',
            barDescription: 'We use our own cookies and third-party cookies so that we can show you a website and understand how you use it, with a view to improving the services we offer. If you continue browsing, we consider that you have accepted the cookies.',
            acceptAll: 'Accept all',
            declineAll: 'Delete all',
            controlCookies: 'Manage cookies',
            unsaved: 'You have unsaved settings',
            close: 'Close',
            save: 'Save',
            necessary: 'Necessary cookies',
            optional: 'Optional cookies',
          },
          es: {
            ...
          }
        },

        //else if not multilanguage
        barTitle: 'Cookies Different',
      },
    }],
  ],

  cookies: {
    necessary: {
      cookies: [
        {
          name: 'Default Cookies',
          //if multilanguage
          description: {
            en: 'Used for cookie control.'
          },

          //else
          description: 'Used for cookie control.'
          cookies: ['cookie_control_consent', 'cookie_control_enabled_cookies']
        }
      ]
    },

    optional: {
      cookies: [
        {
          name: 'Google Analitycs',
          //if multilanguage
          description: {
            en: 'Google Analytics is a web analytics service offered by Google that tracks and reports website traffic.'
          },

          //else
          description: 'Google Analytics is a web analytics service offered by Google that tracks and reports website traffic.'
          src: 'https://www.googletagmanager.com/gtag/js?id=<some-key>',
          async: true,
          cookies: ['_ga', '_gat', '_gid'],
          accepted: () =>{
            (function(){
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', '<some-key>');
            })
          },
          declined: () =>{

          }
        },
      ]
    },
  },
```